import * as restify from 'restify';
import * as jwt from 'jsonwebtoken';
import {User} from '../users/users.model';
import {NotAuthorizedError} from 'restify-errors';
import {environment} from '../common/environment';

export const authenticate: restify.RequestHandler = (req, res, next) => {
    const {email, password} = req.body;
    User.findOne({email}, '+password')
        .then(user => {
            if (user && user.matches(password)) {
                const token = jwt.sign({sub: user.email, iss: 'meat-api'},
                    environment.security.apiSecret);
                res.json({name: user.name, email: user.email, accessToekn: token});
                return next(false);
                // next(false) indica que depois desse processamento,
                // não será necessário mais nenhum outro processamento
            } else {
                return next(new NotAuthorizedError('Invalid Credentials'));
            }
        }).catch(next);
};