import * as restify from 'restify';
import {ForbiddenError} from 'restify-errors';
import {User} from '../users/users.model';

export const authorize: (...profiles: string[]) => restify.RequestHandler = (...profiles) => {
    return (req, res, next) => {
        console.log('>>>>> ' + req['authenticated']);
        console.log('>>>>> ' + profiles);
        User.findOne({email: req['authenticated'].email}).then(user => {
            if (user !== undefined && user.hasAny(...profiles)) {
                next(); // pode passar
            } else {
                next(new ForbiddenError('Permission Denied'));
            }
        });
        /*if (req['authenticated'] !== undefined && req['authenticated'].hasAny(...profiles)) {
            next(); // pode passar
        } else {
            next(new ForbiddenError('Permission Denied'));
        }*/
    };
}