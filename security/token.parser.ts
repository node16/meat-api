import * as restify from 'restify';
import * as jwt from 'jsonwebtoken';
import {environment} from '../common/environment';
import {User} from '../users/users.model';

export const tokenParser: restify.RequestHandler = (req, res, next) => {
    const token = extractToken(req);
    if (token) {
        jwt.verify(token, environment.security.apiSecret, applyBearer(req, next));
    } else {
        next();
    }
};

function extractToken(req: restify.Request) {
    let token = undefined;
    const authorization: string = req.header('authorization');
    if (authorization) {
        const parts: string[] = authorization.split(' ');
        if (parts.length === 2 && parts[0] === 'Bearer') {
            token = parts[1];
        }
    }
    return token;
}

function applyBearer(req: restify.Request, next): (error, decoted) => void {
    return (error, decoted) => {
        if (decoted) {
            User.find({email: decoted.sub}).then(user => {
                if (user) {
                    req['authenticated'] = user;
                }
                next();
            });
        } else {
            next();
        }
    };
}