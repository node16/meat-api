import * as mongoose from 'mongoose';
import {validateCPF} from '../common/validators';
import * as bcrypt from 'bcrypt';
import {environment} from '../common/environment';

/**
 * Em typescript, interfaces não são exportadas como artefatos,
 * elas são usadas apenas para manter um controle estático.
 * Por isso o export da interfaces e do model possuem o mesmo nome.
 */
export interface User extends mongoose.Document {

    name: string;
    email: string;
    password: string;
    cpf: string;
    gender: string;
    profiles: string[]; // perfis (admin, usuário, etc...)
    hasAny(...profiles: string[]): boolean;
    matches(password: string): boolean;

}

/**
 * Usa-se String com ésse maiúsculo por se tratar da String do js e não a string do ts.
 */
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 80,
        minlength: 3
    },
    email: {
        type: String,
        unique: true,
        required: true,
        match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    },
    password: {
        type: String,
        select: false, // não envia o resultado na query
        required: true
    },
    gender: {
        type: String,
        required: false,
        enum: ['Male', 'Female']
    },
    cpf: {
        type: String,
        required: false,
        validate: { // pode ser uma function ao invés de um objeto.
            validator: validateCPF,
            message: '{PATH} Invalid CPF({VALUE})' // PATH e VALUE são variáveis do mongoose
        }
    },
    profiles: {
        type: [String],
        required: false
    }
});

/*
userSchema.statics.myMethod = // cria método estático no schema (sem arrow function)
userSchema.methods.myMethod = // cria método de instânica no schema (sem arrow function)
*/

userSchema.methods.matches = function (password: string): boolean {
    return bcrypt.compareSync(password, this.password);
};

userSchema.methods.hasAny = function (... profiles: String[]): boolean {
    return profiles.some(profile => this.profiles.indexOf(profile) !== -1);
}

// middleware similar ao @PrePersist do JPA :)
userSchema.pre('save', function(next) {
    const user: User = this; // pega a referência do Document (não usar arrow function)
    if (!user.isModified('password')) {
        next();
    } else {
        // 1- valor original, 2- qtde de salts
        bcrypt.hash(user.password, environment.security.saltRounds).then(hash => {
           user.password = hash;
           next();
        }).catch(next);
    }
});

// middleware similar ao @PreUpdate do JPA :)
userSchema.pre('findOneAndUpdate', function(next) {
    if (!this.getUpdate().password) { // se não tiver 'password' dentro do payload da query
        next();
    } else {
        // 1- valor original, 2- qtde de salts
        bcrypt.hash(this.getUpdate().password, environment.security.saltRounds).then(hash => {
            this.getUpdate().password = hash;
            next();
        }).catch(next);
    }
});

// middleware similar ao @PreUpdate do JPA :)
userSchema.pre('update', function(next) {
    if (!this.getUpdate().password) { // se não tiver 'password' dentro do payload da query
        next();
    } else {
        // 1- valor original, 2- qtde de salts
        bcrypt.hash(this.getUpdate().password, environment.security.saltRounds).then(hash => {
            this.getUpdate().password = hash;
            next();
        }).catch(next);
    }
});

// em model, passa-se o nome do modelo e o segundo parâmetro é o schema que vai ser usado.
// o plural é identificado automaticamente
export const User = mongoose.model<User>('User', userSchema);