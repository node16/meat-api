import 'jest'
import * as request from 'supertest';
import * as HttpStatus from 'http-status-codes'
import {Server} from '../server/server';
import {environment} from '../common/environment';
import {usersRouter} from './users.router';
import {User} from './users.model';

let address: string;
let server: Server;

beforeAll(() => {
    environment.db.url = process.env.DB_URL || 'mongodb://172.17.0.2:27017/meat-api-test-db';
    environment.server.port = process.env.SERVER_PORT || 3001;
    address = `http://localhost:${environment.server.port}`;
    server = new Server();
    return server.bootstrap([usersRouter])
        .then(() => User.remove({}).exec()) // limpa a tabela de users no banco de teste
        .catch(console.error);
});

afterAll(() => {
    return server.shutdown();
});

test('get /users', () => {
    return request(address)
        .get('/users')
        .then(response => {
            expect(response.status).toBe(HttpStatus.OK);
        }).catch(fail);
});

test('post /users', () => {
    return request(address)
        .post('/users')
        .send({
            name: 'usuario1',
            email: 'usuario1@email.com',
            password: '123456',
            cpf: '01234567890'
        }).then(response => {
            expect(response.status).toBe(HttpStatus.OK);
            expect(response.body['_id']).toBeDefined();
            expect(response.body['name']).toBe('usuario1');
            expect(response.body['email']).toBe('usuario1@email.com');
            expect(response.body['password']).toBeUndefined();
            expect(response.body['cpf']).toBe('01234567890');
        }).catch(fail);
});

test('get /users/aaaa - not found', () => {
    return request(address)
        .get('/users/aaaaa')
        .then(response => {
            expect(response.status).toBe(HttpStatus.NOT_FOUND);
        }).catch(fail);
});

test('put /users/:id - not found', () => {
    return request(address)
        .post('/users')
        .send({
            name: 'usuario2',
            email: 'usuario2@email.com',
            password: '123456'
        })
        .then(response => request(address)
            .patch(`/users/${response.body['_id']}`)
            .send({
                name: 'usuario2 - atualizado'
            })
        )
        .then(response => {
            expect(response.status).toBe(HttpStatus.OK);
            expect(response.body['_id']).toBeDefined();
            expect(response.body['name']).toBe('usuario2 - atualizado');
            expect(response.body['email']).toBe('usuario2@email.com');
            expect(response.body['password']).toBeUndefined();
        }).catch(fail);
});
