import * as restify from 'restify';
import {User} from './users.model';
import {ModelRouter} from '../common/model-router';
import {authenticate} from '../security/auth.handler';
import {authorize} from '../security/authz.handler';

class UsersRouter extends ModelRouter<User> {

    constructor() {
        super(User);
        this.event.on('beforeRender', document => {
            document.password = undefined;
            // ou delete document.password;
        });
    }

    findByEmail = (req, res, next) => {
        if (req.query['email']) {
            User.find({email: req.query['email']})
                .then(this.renderAll(res, next))
                .catch(next);
        } else {
            next();
        }
    };

    applyRoutes(application: restify.Server) {

        /* no segundo parametro pode passar uma callbak ou um array de callback */
        /* a versão pode ser especificada pelo header "accept-version" */
        /* pode ser especificada uma versão condicional, maior que 1.0 por ex ">1.0.0" */
        /* se não for especificado a versão, o restify usa a mais recente */
        // application.get('/users', this.findAll);
        application.get({path: '/users', version: '1.0.0'}, this.findAll);
        // application.get({path: '/users', version: '2.0.0'}, [authorize('admin'), this.findByEmail, this.findAll]);
        application.get("/users/:id", [this.validateId, this.findById]);
        application.post('/users', this.save);
        application.put('/users/:id', [this.validateId, this.update]);
        application.patch('/users/:id', [this.validateId, this.patch]);
        application.del('/users/:id', [this.validateId, this.remove]);

        application.post('/users/authenticate', authenticate);
    }

}

export const usersRouter = new UsersRouter();