"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const users_model_1 = require("./users.model");
const model_router_1 = require("../common/model-router");
const auth_handler_1 = require("../security/auth.handler");
class UsersRouter extends model_router_1.ModelRouter {
    constructor() {
        super(users_model_1.User);
        this.findByEmail = (req, res, next) => {
            if (req.query['email']) {
                users_model_1.User.find({ email: req.query['email'] })
                    .then(this.renderAll(res, next))
                    .catch(next);
            }
            else {
                next();
            }
        };
        this.event.on('beforeRender', document => {
            document.password = undefined;
            // ou delete document.password;
        });
    }
    applyRoutes(application) {
        /* no segundo parametro pode passar uma callbak ou um array de callback */
        /* a versão pode ser especificada pelo header "accept-version" */
        /* pode ser especificada uma versão condicional, maior que 1.0 por ex ">1.0.0" */
        /* se não for especificado a versão, o restify usa a mais recente */
        // application.get('/users', this.findAll);
        application.get({ path: '/users', version: '1.0.0' }, this.findAll);
        // application.get({path: '/users', version: '2.0.0'}, [authorize('admin'), this.findByEmail, this.findAll]);
        application.get("/users/:id", [this.validateId, this.findById]);
        application.post('/users', this.save);
        application.put('/users/:id', [this.validateId, this.update]);
        application.patch('/users/:id', [this.validateId, this.patch]);
        application.del('/users/:id', [this.validateId, this.remove]);
        application.post('/users/authenticate', auth_handler_1.authenticate);
    }
}
exports.usersRouter = new UsersRouter();
