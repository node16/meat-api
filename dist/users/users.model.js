"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const validators_1 = require("../common/validators");
const bcrypt = require("bcrypt");
const environment_1 = require("../common/environment");
/**
 * Usa-se String com ésse maiúsculo por se tratar da String do js e não a string do ts.
 */
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 80,
        minlength: 3
    },
    email: {
        type: String,
        unique: true,
        required: true,
        match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    },
    password: {
        type: String,
        select: false,
        required: true
    },
    gender: {
        type: String,
        required: false,
        enum: ['Male', 'Female']
    },
    cpf: {
        type: String,
        required: false,
        validate: {
            validator: validators_1.validateCPF,
            message: '{PATH} Invalid CPF({VALUE})' // PATH e VALUE são variáveis do mongoose
        }
    },
    profiles: {
        type: [String],
        required: false
    }
});
/*
userSchema.statics.myMethod = // cria método estático no schema (sem arrow function)
userSchema.methods.myMethod = // cria método de instânica no schema (sem arrow function)
*/
userSchema.methods.matches = function (password) {
    return bcrypt.compareSync(password, this.password);
};
userSchema.methods.hasAny = function (...profiles) {
    return profiles.some(profile => this.profiles.indexOf(profile) !== -1);
};
// middleware similar ao @PrePersist do JPA :)
userSchema.pre('save', function (next) {
    const user = this; // pega a referência do Document (não usar arrow function)
    if (!user.isModified('password')) {
        next();
    }
    else {
        // 1- valor original, 2- qtde de salts
        bcrypt.hash(user.password, environment_1.environment.security.saltRounds).then(hash => {
            user.password = hash;
            next();
        }).catch(next);
    }
});
// middleware similar ao @PreUpdate do JPA :)
userSchema.pre('findOneAndUpdate', function (next) {
    if (!this.getUpdate().password) {
        next();
    }
    else {
        // 1- valor original, 2- qtde de salts
        bcrypt.hash(this.getUpdate().password, environment_1.environment.security.saltRounds).then(hash => {
            this.getUpdate().password = hash;
            next();
        }).catch(next);
    }
});
// middleware similar ao @PreUpdate do JPA :)
userSchema.pre('update', function (next) {
    if (!this.getUpdate().password) {
        next();
    }
    else {
        // 1- valor original, 2- qtde de salts
        bcrypt.hash(this.getUpdate().password, environment_1.environment.security.saltRounds).then(hash => {
            this.getUpdate().password = hash;
            next();
        }).catch(next);
    }
});
// em model, passa-se o nome do modelo e o segundo parâmetro é o schema que vai ser usado.
// o plural é identificado automaticamente
exports.User = mongoose.model('User', userSchema);
