"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const model_router_1 = require("../common/model-router");
const restaurants_model_1 = require("./restaurants.model");
const restify_errors_1 = require("restify-errors");
class RestaurantsRouter extends model_router_1.ModelRouter {
    constructor() {
        super(restaurants_model_1.Restaurant);
        this.findMenu = (req, res, next) => {
            // segundo parâmetro (projeção) funciona como eager fech JPA
            restaurants_model_1.Restaurant.findById(req.params['id'], '+menu').then(restaurant => {
                if (!restaurant) {
                    throw new restify_errors_1.NotFoundError('Restaurant not found');
                }
                res.json(restaurant.menu);
                return next();
            }).catch(next);
        };
        this.updateMenu = (req, res, next) => {
            restaurants_model_1.Restaurant.findById(req.params['id']).then(restaurant => {
                if (!restaurant) {
                    throw new restify_errors_1.NotFoundError('Restaurant not found');
                }
                else {
                    restaurant.menu = req.body; // array de menuitem
                    return restaurant.save();
                }
            }).then(rest => {
                res.json(rest.menu);
                return next();
            }).catch(next);
        };
    }
    applyRoutes(application) {
        /* no segundo parametro pode passar uma callbak ou um array de callbacks */
        application.get('/restaurants', this.findAll);
        application.get("/restaurants/:id", [this.validateId, this.findById]);
        application.post('/restaurants', this.save);
        application.put('/restaurants/:id', [this.validateId, this.update]);
        application.patch('/restaurants/:id', [this.validateId, this.patch]);
        application.del('/restaurants/:id', [this.validateId, this.remove]);
        /* menu */
        application.get('/restaurants/:id/menu', [this.validateId, this.findMenu]);
        application.put('/restaurants/:id/menu', [this.validateId, this.updateMenu]);
    }
}
exports.RestaurantsRouter = RestaurantsRouter;
exports.restaurantsRouter = new RestaurantsRouter();
