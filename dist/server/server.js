"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const restify = require("restify");
const environment_1 = require("../common/environment");
const mongoose = require("mongoose");
const merge_patch_parser_1 = require("./merge-patch.parser");
const error_handler_1 = require("./error.handler");
const token_parser_1 = require("../security/token.parser");
const fs = require("fs");
class Server {
    initializeDb() {
        // somente para retirar o warning de que ficará deprecated nas próximas versões
        mongoose.Promise = global.Promise;
        return mongoose.connect(environment_1.environment.db.url, { useMongoClient: true });
    }
    initRoutes(routers) {
        return new Promise(((resolve, reject) => {
            try {
                /* modo sem https
                this.application = restify.createServer({
                    name: 'meat-api"',
                    version: '1.0.0'
                });
                */
                // com https
                const optionsServer = {
                    name: 'meat-api"',
                    version: '1.0.0'
                };
                if (environment_1.environment.security.enableHTTPS) {
                    optionsServer.certificate = fs.readFileSync(environment_1.environment.security.certificade),
                        optionsServer.key = fs.readFileSync(environment_1.environment.security.key);
                }
                this.application = restify.createServer(optionsServer);
                this.application.use(restify.plugins.queryParser());
                this.application.use(restify.plugins.bodyParser());
                this.application.use(merge_patch_parser_1.mergePatchBodyParser); // somente se for implantar patch
                this.application.use(token_parser_1.tokenParser);
                // routes
                for (let router of routers) {
                    router.applyRoutes(this.application);
                }
                this.application.listen(environment_1.environment.server.port, () => {
                    resolve(this.application);
                });
                // retorna um objeto padronizado de erro: arquivp error.handler.ts
                this.application.on('restifyError', error_handler_1.handleError);
            }
            catch (error) {
                reject(error);
            }
        }));
    }
    bootstrap(routers = []) {
        return this.initializeDb().then(() => this.initRoutes(routers).then(() => this));
    }
    shutdown() {
        return mongoose.disconnect()
            .then(() => this.application.close());
    }
}
exports.Server = Server;
