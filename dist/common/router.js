"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const restify_errors_1 = require("restify-errors");
class Router {
    constructor() {
        this.event = new events_1.EventEmitter();
    }
    render(response, next) {
        return (document) => {
            if (document) {
                this.event.emit('beforeRender', document);
                response.json(document);
            }
            else {
                // response.send(HttpStatus.NOT_FOUND);
                throw new restify_errors_1.NotFoundError('Documento não encontrado');
            }
            return next(false);
        };
    }
    renderAll(response, next) {
        return (documents) => {
            if (documents) {
                documents.forEach(document => {
                    this.event.emit('beforeRender', document);
                });
                response.json(documents);
            }
            else {
                response.json([]);
            }
            return next(false);
        };
    }
}
exports.Router = Router;
