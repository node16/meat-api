"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const router_1 = require("./router");
const restify_errors_1 = require("restify-errors");
const HttpStatus = require("http-status-codes");
class ModelRouter extends router_1.Router {
    constructor(model) {
        super();
        this.model = model;
        this.pageSize = 4;
        this.validateId = (req, res, next) => {
            if (!mongoose.Types.ObjectId.isValid(req.params['id'])) {
                return next(new restify_errors_1.NotFoundError('Document not found'));
            }
            return next();
        };
        this.findPageable = (req, res, next) => {
            let page = parseInt(req.query['_page'] || 1);
            page = page > 0 ? page : 1;
            const skip = (page - 1) * this.pageSize;
            this.model.find()
                .skip(skip)
                .limit(this.pageSize)
                .then(this.renderAll(res, next))
                .catch(next);
        };
        this.findAll = (req, res, next) => {
            this.model.find()
                .then(this.renderAll(res, next))
                .catch(next);
        };
        this.findById = (req, res, next) => {
            this.model.findById(req.params['id']).then(this.render(res, next)).catch(next);
        };
        this.save = (req, res, next) => {
            let document = new this.model(req.body);
            document.save().then(model => {
                if (document['password']) {
                    document['password'] = undefined;
                }
                res.json(model);
                return next();
            }).catch(next);
            // document.save().then(this.render(req, next)).catch(next);
        };
        this.update = (req, res, next) => {
            const options = { runValidators: true, overwrite: true };
            // o update retorna a qtde de registros atualizados na prop 'n'
            this.model.update({ _id: req.params['id'] }, req.body, options)
                .exec().then(result => {
                if (result.n) {
                    return this.model.findById(req.params['id']);
                }
                else {
                    return res.send(HttpStatus.NOT_FOUND);
                    // throw new NotFoundError('Documento não encontrado');
                }
            }).then(user => {
                res.json(user);
                return next();
            }).catch(next);
        };
        this.patch = (req, res, next) => {
            const options = { runValidators: true, new: true }; // new indica para retornar o obj novo e não o antigo
            this.model.findByIdAndUpdate(req.params['id'], req.body, options).then(user => {
                if (user) {
                    res.json(user);
                }
                else {
                    // res.send(HttpStatus.NOT_FOUND);
                    throw new restify_errors_1.NotFoundError('Documento não encontrado');
                }
                return next();
            }).catch(next);
        };
        this.remove = (req, res, next) => {
            this.model.remove({ _id: req.params['id'] }).exec().then((cmdResult) => {
                if (cmdResult.result.n) {
                    res.send(HttpStatus.NO_CONTENT);
                }
                else {
                    // res.send(HttpStatus.NOT_FOUND);
                    throw new restify_errors_1.NotFoundError('Documento não encontrado');
                }
                return next();
            }).catch(next);
        };
    }
}
exports.ModelRouter = ModelRouter;
