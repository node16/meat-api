"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const restify_errors_1 = require("restify-errors");
const users_model_1 = require("../users/users.model");
exports.authorize = (...profiles) => {
    return (req, res, next) => {
        console.log('>>>>> ' + req['authenticated']);
        console.log('>>>>> ' + profiles);
        users_model_1.User.findOne({ email: req['authenticated'].email }).then(user => {
            if (user !== undefined && user.hasAny(...profiles)) {
                next(); // pode passar
            }
            else {
                next(new restify_errors_1.ForbiddenError('Permission Denied'));
            }
        });
        /*if (req['authenticated'] !== undefined && req['authenticated'].hasAny(...profiles)) {
            next(); // pode passar
        } else {
            next(new ForbiddenError('Permission Denied'));
        }*/
    };
};
