"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("jsonwebtoken");
const users_model_1 = require("../users/users.model");
const restify_errors_1 = require("restify-errors");
const environment_1 = require("../common/environment");
exports.authenticate = (req, res, next) => {
    const { email, password } = req.body;
    users_model_1.User.findOne({ email }, '+password')
        .then(user => {
        if (user && user.matches(password)) {
            const token = jwt.sign({ sub: user.email, iss: 'meat-api' }, environment_1.environment.security.apiSecret);
            res.json({ name: user.name, email: user.email, accessToekn: token });
            return next(false);
            // next(false) indica que depois desse processamento,
            // não será necessário mais nenhum outro processamento
        }
        else {
            return next(new restify_errors_1.NotAuthorizedError('Invalid Credentials'));
        }
    }).catch(next);
};
