"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const model_router_1 = require("../common/model-router");
const reviews_model_1 = require("./reviews.model");
class ReviewsRouter extends model_router_1.ModelRouter {
    constructor() {
        super(reviews_model_1.Review);
        // @Override
        this.findById = (req, res, next) => {
            this.model.findById(req.params['id'])
                .populate('user', 'name') // popula somente o name de user - eager JPA
                .populate('restaurant') // popula todos de restaurant - eager JPA
                .then(this.render(res, next)).catch(next);
        };
    }
    applyRoutes(application) {
        /* no segundo parametro pode passar uma callbak ou um array de callbacks */
        application.get('/reviews', this.findAll);
        application.get("/reviews/:id", [this.validateId, this.findById]);
        application.post('/reviews', this.save);
    }
}
exports.ReviewsRouter = ReviewsRouter;
exports.reviewsRouter = new ReviewsRouter();
