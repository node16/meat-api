import * as mongoose from 'mongoose';

export interface MenuItem extends mongoose.Document {
    name: string;
    price: number
}

export interface Restaurant extends mongoose.Document {
    name: string;
    menu: MenuItem[]
}

const menuSchema = new mongoose.Schema({
    name: {
        type: String
    },
    price: {
        type: Number,
        required: true
    }
});

const restaurantSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    menu: {
        type: [menuSchema], // mapeamento
        required: false,
        select: false, // similar ao lazy fetch do JPA
        default: []
    }
});

export const Restaurant = mongoose.model<Restaurant>('Restaurant', restaurantSchema);