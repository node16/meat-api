import {ModelRouter} from '../common/model-router';
import {Restaurant} from './restaurants.model';
import * as restify from 'restify';
import {NotFoundError} from 'restify-errors';

export class RestaurantsRouter extends ModelRouter<Restaurant> {

    constructor() {
        super(Restaurant);
    }

    findMenu = (req, res, next) => {
        // segundo parâmetro (projeção) funciona como eager fech JPA
        Restaurant.findById(req.params['id'], '+menu').then(restaurant => {
            if (!restaurant) {
                throw new NotFoundError('Restaurant not found');
            }
            res.json(restaurant.menu);
            return next();
        }).catch(next);
    };

    updateMenu = (req, res, next) => {
        Restaurant.findById(req.params['id']).then(restaurant => {
            if (!restaurant) {
                throw new NotFoundError('Restaurant not found');
            } else {
                restaurant.menu = req.body; // array de menuitem
                return restaurant.save();
            }
        }).then(rest => {
            res.json(rest.menu);
            return next();
        }).catch(next);
    };

    applyRoutes(application: restify.Server) {

        /* no segundo parametro pode passar uma callbak ou um array de callbacks */
        application.get('/restaurants', this.findAll);
        application.get("/restaurants/:id", [this.validateId, this.findById]);
        application.post('/restaurants', this.save);
        application.put('/restaurants/:id', [this.validateId, this.update]);
        application.patch('/restaurants/:id', [this.validateId, this.patch]);
        application.del('/restaurants/:id', [this.validateId, this.remove]);

        /* menu */
        application.get('/restaurants/:id/menu', [this.validateId, this.findMenu]);
        application.put('/restaurants/:id/menu', [this.validateId, this.updateMenu]);
    }

}

export const restaurantsRouter = new RestaurantsRouter();