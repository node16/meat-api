import * as restify from 'restify';
import {EventEmitter} from 'events';
import {NotFoundError} from 'restify-errors';

export abstract class Router {

    event: EventEmitter = new EventEmitter();

    abstract applyRoutes(application: restify.Server);

    render(response: restify.Response, next: restify.Next) {
        return (document) => {
            if (document) {
                this.event.emit('beforeRender', document);
                response.json(document);
            } else {
                // response.send(HttpStatus.NOT_FOUND);
                throw new NotFoundError('Documento não encontrado');
            }
            return next(false);
        }
    }

    renderAll(response: restify.Response, next: restify.Next) {
        return (documents: any[]) => {
            if (documents) {
                documents.forEach(document => {
                    this.event.emit('beforeRender', document);
                });
                response.json(documents);
            } else {
                response.json([]);
            }
            return next(false);
        }
    }

}