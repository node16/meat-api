import * as restify from 'restify';
import {environment} from '../common/environment';
import {Router} from '../common/router';
import * as mongoose from 'mongoose';
import {mergePatchBodyParser} from './merge-patch.parser'
import {handleError} from './error.handler';
import {tokenParser} from '../security/token.parser';
import * as fs from 'fs';
import {logger} from '../common/logger';

export class Server {

    application: restify.Server;

    initializeDb(): mongoose.MongooseThenable {
        // somente para retirar o warning de que ficará deprecated nas próximas versões
        (<any>mongoose).Promise = global.Promise;
        return mongoose.connect(environment.db.url, {useMongoClient: true});
    }

    initRoutes(routers: Router[]): Promise<any> {
        return new Promise<any>(((resolve, reject) => {
            try {
                /* modo sem https
                this.application = restify.createServer({
                    name: 'meat-api"',
                    version: '1.0.0'
                });
                */

                // com https
                const optionsServer: restify.ServerOptions = {
                    name: 'meat-api"',
                    version: '1.0.0',
                    log: logger
                }

                if (environment.security.enableHTTPS) {
                    optionsServer.certificate = fs.readFileSync(environment.security.certificade),
                    optionsServer.key = fs.readFileSync(environment.security.key)
                }

                this.application = restify.createServer(optionsServer);

                // fica disponível em "request.log.debug('teste')"
                // pode-se usar  %s(string) %j(json) para formatar o log
                this.application.pre(restify.plugins.requestLogger({
                    log: logger
                }));

                this.application.use(restify.plugins.queryParser());
                this.application.use(restify.plugins.bodyParser());
                this.application.use(mergePatchBodyParser); // somente se for implantar patch
                this.application.use(tokenParser);

                // routes
                for (let router of routers) {
                    router.applyRoutes(this.application);
                }

                this.application.listen(environment.server.port, () => {
                    resolve(this.application);
                });

                // retorna um objeto padronizado de erro: arquivp error.handler.ts
                this.application.on('restifyError', handleError);

            } catch (error) {
                reject(error);
            }
        }));
    }

    bootstrap(routers: Router[] = []): Promise<Server> {
        return this.initializeDb().then(() =>
            this.initRoutes(routers).then(() => this));
    }

    shutdown() {
        return mongoose.disconnect()
            .then(() => this.application.close());
    }

}