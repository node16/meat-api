import * as restify from 'restify';
import * as HttpStatus from 'http-status-codes';

export const handleError = (req: restify.Request, res: restify.Response, err, done) => {
    err.toJson = () => {
        return {
            message: err.message
        }
    };

    switch (err.name) {
        case 'MongoError':
            if (err.code === 11000) {
                err.statusCode = HttpStatus.BAD_REQUEST;
            }
            break;
        case 'ValidationError':
            err.statusCode = HttpStatus.BAD_REQUEST;
            const messages: any = [];
            for (let name in err.errors) {
                messages.push({message: err.errors[name].message});
            }
            err.toJson = () => {
                return {
                    message: 'Validation error while processing your request',
                    errors: messages
                }
            };
            break;
    }

    done();
}