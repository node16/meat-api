import {ModelRouter} from '../common/model-router';
import {Review} from './reviews.model';
import * as restify from 'restify';

export class ReviewsRouter extends ModelRouter<Review> {


    constructor() {
        super(Review);
    }

    // @Override
    findById = (req, res, next) => {
        this.model.findById(req.params['id'])
            .populate('user', 'name') // popula somente o name de user - eager JPA
            .populate('restaurant') // popula todos de restaurant - eager JPA
            .then(this.render(res, next)).catch(next);
    };

    applyRoutes(application: restify.Server) {

        /* no segundo parametro pode passar uma callbak ou um array de callbacks */
        application.get('/reviews', this.findAll);
        application.get("/reviews/:id", [this.validateId, this.findById]);
        application.post('/reviews', this.save);

    }

}

export const reviewsRouter = new ReviewsRouter();
